Word embedding similarity tool
==============================
Benoit Favre 2019


Install
=======

This python script can be used as a library or from the commandline.
It is compatible with python 2 and 3, and uses numpy as only requirement.

To install numpy, you can do it this way:
```
$ pip install --user numpy
```

This program uses textual embeddings in the word2vec format:
```
<num-words> <num-dimentions>
<word1> <value-1> ... <value-n>
...
```

Recommanded: for fast loading, this format can be converted to a binary format:
```
$ python word2vec.py convert <txt-embedding-file> <bin-embedding-file>
```

Compute the similarity between two words
========================================

```
$ python word2vec.py sim <embedding-file> <word1> <word2>
```

It can also read a list of pairs of words from stdin:

```
$ echo -e "dog cat\napple pear\nking queen" | python word2vec.py sim <embedding-file>
```

Compute the closest words to a given word
=========================================

```
$ python word2vec.py closest <embedding-file> <n> <word>
```

`n` is the number of nighbors to show

It can also read a list of words from stdin:

```
$ echo -e "dog\ncat\napple\npear\nking\nqueen" | python word2vec.py closest <embedding-file> 10
```

Provided model
==============

A model for French can be downloaded at:
https://pageperso.lis-lab.fr/benoit.favre/files/most-similar-words-fr.zip

It was trained on French wikipedia with word2vec skipgrams given a window of 30 words.

